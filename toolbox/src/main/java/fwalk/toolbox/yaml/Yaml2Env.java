package fwalk.toolbox.yaml;

import org.yaml.snakeyaml.Yaml;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

public class Yaml2Env {

    private static final Logger LOGGER = Logger.getLogger(Yaml2Env.class.getName());

    public Yaml2Env() {
    }

    public String process(String prefix, Boolean usePrefix, String yaml) {

        String env;
        if (!usePrefix) {
            prefix = "";
        }

        try {
            TreeMap<String, Map<String, Object>> y = new Yaml().loadAs(yaml, TreeMap.class);
            env = toEnv(y, "_", prefix);
        } catch (Exception ex) {
            throw new RuntimeException("Yaml Konvertierungsfehler:" + ex.getMessage());
        }

        TreeMap<String, Map<String, Object>> y = new Yaml().loadAs(yaml, TreeMap.class);
        env = toEnv(y, "_", prefix);
        return env;
    }

    private String fmtEnv(String key) {

        return key.toUpperCase().replaceAll("-", "").replaceAll("\\.", "_");
    }

    private String toEnv(TreeMap<String, Map<String, Object>> config, String delemiter, String prefix) {

        StringBuilder sb = new StringBuilder();
        for (String key : config.keySet()) {
            sb.append(toString(key, config.get(key), delemiter, prefix));
        }
        return sb.toString();
    }

    private String toString(String key, Map<String, Object> map, String delemiter, String prefix) {

        StringBuilder sb = new StringBuilder();
        for (String mapKey : map.keySet()) {
            if (map.get(mapKey) instanceof Map) {
                sb.append(toString(String.format("%s%s%s", fmtEnv(key), delemiter, fmtEnv(mapKey)), (Map<String, Object>) map.get(mapKey), delemiter, prefix));
            } else {
                String pfx = (!prefix.isEmpty()) ? prefix + delemiter : "";
                sb.append(String.format("%s%s%s%s=%s%n", pfx, fmtEnv(key), delemiter, fmtEnv(mapKey), map.get(mapKey).toString()));
            }
        }
        return sb.toString();
    }
}
