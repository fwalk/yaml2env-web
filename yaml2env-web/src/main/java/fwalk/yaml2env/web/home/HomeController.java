package fwalk.yaml2env.web.home;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.logging.Logger;

@Controller
class HomeController {

    private static final Logger LOGGER = Logger.getLogger(HomeController.class.getName());

    @GetMapping("/")
    String index(Model model) {
        return "index";

    }

    @GetMapping("/r1")
    public RedirectView redirect_1(RedirectAttributes attributes) {
        attributes.addFlashAttribute("flash_now", LocalDateTime.now());
        attributes.addAttribute("now", LocalDateTime.now());
        return new RedirectView("http://127.0.0.11:8080/about");
    }

    @GetMapping("/r2")
    public ModelAndView redirect_2(Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return new ModelAndView("redirect:http://127.0.0.11:8080/about");
    }

    @GetMapping("/about")
    String about(Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "about";
    }

    @GetMapping("/properties")
    @ResponseBody
    java.util.Properties properties() {
        return System.getProperties();
    }

    @GetMapping("/headers")
    @ResponseBody
    public Map<String,String> shwoHeaders(@RequestHeader HttpHeaders headers) {
        return headers.toSingleValueMap();
    }

}
