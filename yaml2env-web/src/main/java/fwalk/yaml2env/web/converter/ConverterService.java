package fwalk.yaml2env.web.converter;

import fwalk.toolbox.yaml.Yaml2Env;
import org.springframework.stereotype.Service;

@Service
public class ConverterService {

    String process(String prefix, Boolean usePrefix, String yaml){

        return new Yaml2Env().process(prefix, usePrefix, yaml);
    }
}
