package fwalk.yaml2env.web.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@Controller
class ConverterController {

    private static final Logger LOGGER = Logger.getLogger(ConverterController.class.getName());

    @Autowired
    private ConverterService converter;

    @GetMapping("/converter")
    String converterForm(Model model) {

        ConverterFormCommand formCommand = new ConverterFormCommand();
        model.addAttribute("formCommand", formCommand);
        model.addAttribute("yamlAsEnv", "1121");
        return "converter";
    }

    @PostMapping("/converter")
    String converterProcess(Model model, @ModelAttribute("formCommand") ConverterFormCommand formCommand) {

        LOGGER.info(String.format("formCommand: %s", formCommand.toString()));

        String yamlAsEnv = "";
        try {
            yamlAsEnv = converter.process(formCommand.prefix, formCommand.usePrefix, formCommand.yaml);
        } catch (Exception ex) {
            LOGGER.info(ex.getMessage());
            formCommand.setEnv("Error: " + ex.getMessage());
        }

        if (!yamlAsEnv.isEmpty()) {
            formCommand.setEnv(yamlAsEnv);
        }
        model.addAttribute("formCommand", formCommand);
        return "converter";
    }
}