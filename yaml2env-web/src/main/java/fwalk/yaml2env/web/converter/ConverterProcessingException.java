package fwalk.yaml2env.web.converter;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Konvierungsfehler")
public class ConverterProcessingException extends Exception {

    private static final long serialVersionUID = 1L;
}
