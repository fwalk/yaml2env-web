package fwalk.yaml2env.web.converter;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ConverterFormCommand {

    String prefix;

    boolean usePrefix;

    String yaml;

    String env;
}
