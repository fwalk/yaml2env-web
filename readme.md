
# Run, build continuously

Idea supports spring-boot only in ultimate, workaround this with:

Terminal 1:

```plain
./gradlew build --continuous
```

Terminal 2:

```plain
./gradlew bootRun
```

# JIB - Docker build _with_ docker

```plain
./gradlew clean jibDockerBuild
```

Verify:

```plain
docker run --rm -d -p 8080:8080 yaml2env-web:latest
```

# JIB - Docker build _without_ docker

```plain
./gradlew clean jib
```

Registry host, user, password must be set eventually.


# References:

- https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin
- https://github.com/nebula-plugins/gradle-info-plugin/blob/master/src/main/groovy/nebula/plugin/info/scm/ScmInfoPlugin.groovy

- http://andresalmiray.com/customize-jar-manifest-entries-with-maven-gradle/